import os
import torch
from diffusers import DiffusionPipeline

base_dir = "/content"

pipe = DiffusionPipeline.from_pretrained(
   "playgroundai/playground-v2-1024px-aesthetic",
   torch_dtype=torch.float16,
   variant="fp16"
).to("cuda")

prompts = [
    "Astronaut in a jungle, cold color palette, muted colors, detailed, 8k",
    "Surreal cityscape with neon lights, futuristic vibe",
    "Vintage car in a desert landscape, warm tones, dramatic lighting"
]

for i, prompt in enumerate(prompts):
    image = pipe(prompt).images[0]

    # Save to base dir
    image_path = os.path.join(base_dir, f"generated_image_{i}.png")
    image.save(image_path)

    print(f"Saved image {i+1}/{len(prompts)} to {image_path}")